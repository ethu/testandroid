package com.example.testandroid;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Window;
import android.widget.TabHost;

public class HccFragmentActivity extends FragmentActivity {
	private static int currentLayout = 0;
	TabHost tabhost;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_hcc_fragment);
		
		setFragment();
		changeLayout();
	}
	
	private void setFragment(){
		tabhost = (TabHost) findViewById(R.id.tabhost);
		tabhost.setup();
		tabhost.addTab(tabhost.newTabSpec("conversation").setIndicator("会话", 
				this.getResources().getDrawable(R.drawable.ic_setting)).setContent(R.id.converstation));
		tabhost.addTab(tabhost.newTabSpec("contact").setIndicator("联系人", 
				this.getResources().getDrawable(R.drawable.ic_programme)).setContent(R.id.contact));
		tabhost.setCurrentTab(0);
	}

	private void changeLayout(){
		tabhost.setCurrentTab(currentLayout);
	}


}
