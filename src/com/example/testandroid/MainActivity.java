package com.example.testandroid;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;

import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends SherlockActivity {
	Button btn1;
	Button btn2;
	Button btn3;
	Button btn4;
	Button btn5;
	Button btn6;
	Button btn7;
	Button btn8;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		getSupportActionBar().setTitle("hi");
		setTheme(com.actionbarsherlock.R.style.Theme_Sherlock);
		btn1 = (Button) findViewById(R.id.btn1);
		btn1.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MainActivity.this, OptionMenuActivity.class);
				startActivity(intent);
			}
		});
		
		btn2 = (Button) findViewById(R.id.btn2);
		btn2.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MainActivity.this, GalleryActivity.class);
				startActivity(intent);
			}
			
		});
		
		btn3 = (Button) findViewById(R.id.btn3);
		btn3.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MainActivity.this, TestTableLayoutActivity.class);
				startActivity(intent);
			}
		});
		
		btn4 = (Button) findViewById(R.id.btn4);
		btn4.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MainActivity.this, ZhihuaActivity.class);
				startActivity(intent);
			}
		});
		
		btn5 = (Button) findViewById(R.id.btn5);
		btn5.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MainActivity.this, LoadMoreActivity.class);
				startActivity(intent);
			}
		});
		
		btn6 = (Button) findViewById(R.id.btn6);
		btn6.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MainActivity.this, WebViewActivity.class);
				startActivity(intent);
			}
		});
		
		btn7 = (Button) findViewById(R.id.btn7);
		btn7.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MainActivity.this, HccFragmentActivity.class);
				startActivity(intent);
			}
		});
		
		btn8 = (Button) findViewById(R.id.btn8);
		btn8.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MainActivity.this, ApiFragmentActivity.class);
				startActivity(intent);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
