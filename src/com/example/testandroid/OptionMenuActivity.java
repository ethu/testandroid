package com.example.testandroid;

import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.adsmogo.adapters.AdsMogoCustomEventPlatformEnum;
import com.adsmogo.adview.AdsMogoLayout;
import com.adsmogo.controller.listener.AdsMogoListener;
import com.guohead.sdk.GHView;
import com.guohead.sdk.GHView.OnAdLoadedListener;

public class OptionMenuActivity extends SherlockActivity implements AdsMogoListener{
	private AdsMogoLayout adsMogo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_option_menu);
		setTheme(com.actionbarsherlock.R.style.Theme_Sherlock);
		getSupportActionBar().setTitle("hi");
		
		adsMogo = (AdsMogoLayout) findViewById(R.id.mogoView);
		adsMogo.setAdsMogoListener(this);
		
		GHView ghView = (GHView) findViewById(R.id.ghView);
		ghView.setAdUnitId(getString(R.string.guohead_unitid));
		ghView.startLoadAd();
		
		// 监听广告成功展示
		ghView.setOnAdLoadedListener(new OnAdLoadedListener() {
			@Override
			public void OnAdLoaded(GHView ghView) {
				// TODO Auto-generated method stub
				Log.i("suc" , "guohe  : OnAdLoaded()");
			}

		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.option_menu, menu);
		//super.onCreateOptionsMenu(menu);
		menu.add(Menu.NONE, Menu.FIRST+1, 5, "delete")
		.setIcon(R.drawable.ic_setting)
		.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		menu.add(Menu.NONE, Menu.FIRST+2, 2, "save")
		.setIcon(R.drawable.ic_programme)
		.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		menu.add(Menu.NONE, Menu.FIRST+3, 6, "help")
		.setIcon(R.drawable.ic_more)
		.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
		case Menu.FIRST+1:
			Toast.makeText(this, "delete is clicked", Toast.LENGTH_LONG).show();
			return true;
		case Menu.FIRST+2:
			Toast.makeText(this, "save is clicked", Toast.LENGTH_LONG).show();
		    return true;
		case Menu.FIRST+3:
			Toast.makeText(this, "help is clicked", Toast.LENGTH_LONG).show();
		    return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public Class getCustomEvemtPlatformAdapterClass(
			AdsMogoCustomEventPlatformEnum arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onClickAd(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onCloseAd() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onCloseMogoDialog() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFailedReceiveAd() {
		// TODO Auto-generated method stub
		Log.d("suc", "receive ads fail");
	}

	@Override
	public void onRealClickAd() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceiveAd(ViewGroup arg0, String arg1) {
		// TODO Auto-generated method stub
		Log.d("suc", "receive mang guoads success");
		
	}

	@Override
	public void onRequestAd(String arg0) {
		// TODO Auto-generated method stub
		
	}
	
    protected void onDestroy(){
    	AdsMogoLayout.clear();
        super.onDestroy();
    }

}
