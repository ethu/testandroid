package com.example.testandroid;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class TestTableLayoutActivity extends Activity {
	private final int WC = ViewGroup.LayoutParams.WRAP_CONTENT;
	private final int FP = ViewGroup.LayoutParams.FILL_PARENT;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test_table_layout);
        //新建TableLayout01的实例
        TableLayout tableLayout = (TableLayout)findViewById(R.id.TableLayout01);
        //全部列自动填充空白处
        tableLayout.setStretchAllColumns(true);
        //生成10行，8列的表格
        for(int row=0;row<10;row++)
        {
            TableRow tableRow=new TableRow(this);
            for(int col=0; col < 8; col++){
            	//tv用于显示
            	TextView tv=new TextView(this);
                tv.setText("("+col+","+row+")");
                tableRow.addView(tv);
            }
            //新建的TableRow添加到TableLayout
            tableLayout.addView(tableRow, new TableLayout.LayoutParams(FP, WC));
        }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.test_table_layout, menu);
		return true;
	}

}
