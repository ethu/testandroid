package com.example.testandroid;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Gallery;
import android.widget.ImageView;

public class GalleryActivity extends Activity {
	Gallery gallery;
	ImageView photo;
	GalleryAdapter ga;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gallery);
		
		ga = new GalleryAdapter(this);
		gallery = (Gallery) findViewById(R.id.gallery);
		photo = (ImageView) findViewById(R.id.gallery_photo);
		//��adapter
		gallery.setAdapter(ga);
		
		gallery.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				photo.setBackgroundResource(ga.images[arg2]);
			}  
		});
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.gallery, menu);
		return true;
	}

}
