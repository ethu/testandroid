package com.example.testandroid;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebViewActivity extends Activity {
	private WebView webview;
	final Activity activity = this;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		webview = new WebView(this);
		setContentView(webview);
		
		webview.getSettings().setJavaScriptEnabled(true);
		
		webview.loadUrl("http://www.tudou.com");
		
		webview.setWebViewClient(new HelloWebViewClient());
		webview.setWebChromeClient(new HelloWebChromeClient());
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		super.onKeyDown(keyCode, event);
		if ((keyCode == KeyEvent.KEYCODE_BACK) && webview.canGoBack()) {
			webview.goBack(); // goBack()表示返回WebView的上一页面
			return true;
		}
		return false;
	}

	// Web视图
	private class HelloWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}
	}

	private class HelloWebChromeClient extends WebChromeClient {
//		private View videoProgressView;
//		private Bitmap videoDefaultBitmap;
//		
//		public void onProgressChanged(WebView view, int progress) {
//			activity.setTitle("Loading...");
//			activity.setProgress(progress * 100);
//			if (progress == 100){
//				activity.setTitle(R.string.app_name);
//			}
//		}
//		
//		@Override
//		public View getVideoLoadingProgressView(){
//			if(videoProgressView == null){
//				videoProgressView = LayoutInflater.from(activity).inflate(R.layout.video_loading_progress, null);
//			}
//			
//			return videoProgressView;
//		}
//		
//		@Override
//		public Bitmap getDefaultVideoPoster(){
//			if(videoDefaultBitmap == null){
//				videoDefaultBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.default_video_poster);
//			}
//			return videoDefaultBitmap;
//		}
		
		WebChromeClient.CustomViewCallback mCallback;
		View mView;
		
		@Override
		public void onShowCustomView(View view, WebChromeClient.CustomViewCallback callback){
			if(mCallback != null){
				mCallback.onCustomViewHidden();
				mCallback = null;
				return;
			}
			ViewGroup parent = (ViewGroup) webview.getParent();
			parent.removeView(webview);
			//view可能是video player view???
			//callback回调函数给的接口，可以关掉video player
			parent.addView(view);
			mView = view;
			mCallback = callback;
			
			//super.onShowCustomView(view, callback);
		}
		
		@Override
		public void onHideCustomView(){
			if(mView != null){
				if(mCallback != null){
					mCallback.onCustomViewHidden();
					mCallback = null;
				}
				ViewGroup parent = (ViewGroup) mView.getParent();
				parent.removeView(mView);
				parent.addView(webview);
				mView = null;
			}
		}
	}
}
