package com.example.testandroid;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;


public class GalleryAdapter extends BaseAdapter {
	public Integer[] images = {R.drawable.ic_launcher, R.drawable.ic_more, R.drawable.ic_programme, R.drawable.ic_setting};
	Context context;

	public GalleryAdapter(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return images.length;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		//ImageView必须有上下文？？？？
		//?????
		ImageView iv = new ImageView(context);
		iv.setImageResource(images[arg0]);
		return iv;
	}

}
