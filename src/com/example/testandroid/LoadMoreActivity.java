package com.example.testandroid;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.testandroid.LoadMoreListView.OnLoadListener;

/**
 * 点击加载更多
 * @author new
 *
 */
public class LoadMoreActivity extends Activity {
	private List<AppInfo> mList = new ArrayList<AppInfo>();
	private CustomListAdapter mAdapter;
	private int count = 10;
	private LoadMoreListView mListView;
	private static final int LOAD_DATA_FINISH = 10;
	private Handler handler = new Handler(){
		public void handleMessage(android.os.Message msg){
			if(msg.what == LOAD_DATA_FINISH){
				if(mAdapter != null){
					mAdapter.notifyDataSetChanged();
				}
				mListView.loadComplete();
			}
		}
	};;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_load_more);
		
		mAdapter = new CustomListAdapter(this);
		mListView = (LoadMoreListView) findViewById(R.id.mListView);
		mListView.setAdapter(mAdapter);
		
		mListView.setonLoadListener(new OnLoadListener(){

			@Override
			public void onLoad() {
				// TODO Auto-generated method stub
				loadMoreData();
			}
			
		});
		
		
		buildAppData();
		
	
	}
	
	private void loadMoreData(){
		new Thread(){
			public void run(){
				for(int i=count;i<count+10;i++){
					AppInfo ai = new AppInfo();

					ai.setAppIcon(BitmapFactory.decodeResource(getResources(),
							R.drawable.ic_launcher));
					ai.setAppName("应用Demo_" + i);
					ai.setAppVer("版本: " + (i % 10 + 1) + "." + (i % 8 + 2) + "."
							+ (i % 6 + 3));
					ai.setAppSize("大小: " + i * 10 + "MB");

					//这个地方重要！
					//增加数据的地方！！！
					mList.add(ai);
				}
				count += 10;
				
				handler.sendEmptyMessage(LOAD_DATA_FINISH);
			}
		}.start();
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.load_more, menu);
		return true;
	}
	
	private void buildAppData() {
		for (int i = 0; i < 10; i++) {
			AppInfo ai = new AppInfo();

			ai.setAppIcon(BitmapFactory.decodeResource(getResources(),
					R.drawable.ic_launcher));
			ai.setAppName("应用Demo_" + i);
			ai.setAppVer("版本: " + (i % 10 + 1) + "." + (i % 8 + 2) + "."
					+ (i % 6 + 3));
			ai.setAppSize("大小: " + i * 10 + "MB");

			mList.add(ai);
			
			handler.sendEmptyMessage(LOAD_DATA_FINISH);
		}
	}
	
	public class CustomListAdapter extends BaseAdapter {

		private LayoutInflater mInflater;

		public CustomListAdapter(Context context) {
			mInflater = LayoutInflater.from(context);
		}

		@Override
		public int getCount() {
			return mList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return mList.get(arg0);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (getCount() == 0) {
				return null;
			}

			ViewHolder holder = null;
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.loadmore_item, null);

				holder = new ViewHolder();
				holder.ivImage = (ImageView) convertView
						.findViewById(R.id.ivIcon);
				holder.tvName = (TextView) convertView
						.findViewById(R.id.tvName);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			AppInfo ai = mList.get(position);
			holder.ivImage.setImageBitmap(ai.getAppIcon());
			holder.tvName.setText(ai.getAppName());

			return convertView;
		}
	}

	public static class ViewHolder {
		private ImageView ivImage;
		private TextView tvName;


	}

}
