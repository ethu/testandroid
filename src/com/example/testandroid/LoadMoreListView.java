package com.example.testandroid;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class LoadMoreListView extends ListView {
	private View moreView;
	private ProgressBar moreProgressBar;
	private TextView loadMoreView;
	private OnLoadListener loadListener;

	public LoadMoreListView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		init(context);
	}

	public LoadMoreListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		init(context);
	}
	
	private void init(Context context){
		moreView = LayoutInflater.from(context).inflate(R.layout.listfooter_more, null);
		moreView.setVisibility(View.VISIBLE);
		moreProgressBar = (ProgressBar) moreView.findViewById(R.id.pull_to_refresh_progress);
		loadMoreView = (TextView) moreView.findViewById(R.id.load_more);
		moreView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				onLoad();
			}
		});
		addFooterView(moreView);
	}
	
	public void setonLoadListener(OnLoadListener loadListener) {
		this.loadListener = loadListener;
	}

	
	
	private void onLoad() {
		if (loadListener != null) {
			moreProgressBar.setVisibility(View.VISIBLE);
			loadMoreView.setText(getContext().getString(R.string.more_data));
			loadListener.onLoad();
		}
	}
	
	public void loadComplete(){
		moreProgressBar.setVisibility(View.GONE);
		loadMoreView.setText(getContext().getString(R.string.more_data));
	}
	
	public interface OnLoadListener {
		public void onLoad();
	}

}
