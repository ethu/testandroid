package com.example.testandroid;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Color;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * 导航栏的手滑效果
 * @author new
 *
 */
public class ZhihuaActivity extends Activity {
	private HorizontalScrollView horizontalScrollView = null;
	int which;
	private ArrayList<TextView> menuList;
	
    /** Called when the activity is first created. */
	private String[] menus = {SlideMenuUtil.ITEM_MOBILE,SlideMenuUtil.ITEM_WEB,
								SlideMenuUtil.ITEM_CLOUD,SlideMenuUtil.ITEM_DATABASE,
								SlideMenuUtil.ITEM_EMBED,SlideMenuUtil.ITEM_SERVER,
								SlideMenuUtil.ITEM_DOTNET,SlideMenuUtil.ITEM_JAVA,
								SlideMenuUtil.ITEM_SAFE,SlideMenuUtil.ITEM_DOMAIN,
								SlideMenuUtil.ITEM_RESEASRCH,SlideMenuUtil.ITEM_MANAGE};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_zhihua);
		
		horizontalScrollView = (HorizontalScrollView)findViewById(R.id.horizonMenu);
	       
        setSlideMenu();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.zhihua, menu);
		return true;
	}
	
	private void setSlideMenu(){
    	// 包含TextView的LinearLayout
		LinearLayout menuLinerLayout = (LinearLayout) findViewById(R.id.linearLayoutMenu);
		//menuLinerLayout.setOrientation(LinearLayout.HORIZONTAL);
		// 参数设置
		LinearLayout.LayoutParams menuLinerLayoutParames = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT, 
				LinearLayout.LayoutParams.WRAP_CONTENT,
				1);
		menuLinerLayoutParames.gravity = Gravity.CENTER_HORIZONTAL;
		
		menuList = new ArrayList<TextView>(menus.length);
		// 添加TextView控件
    	for(int i = 0;i < menus.length;i++){
    		TextView tvMenu = new TextView(this);
			tvMenu.setLayoutParams(new LayoutParams(30,30)); 
			tvMenu.setPadding(30, 14, 30, 10);
			tvMenu.setTag(menus[i]);
			tvMenu.setText(menus[i]);
			tvMenu.setTextColor(Color.WHITE);
			tvMenu.setGravity(Gravity.CENTER_HORIZONTAL);
			menuList.add(tvMenu);
			tvMenu.setOnClickListener(new OnClickListener(){
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					TextView clickWho = (TextView)arg0;
					TextView text = (TextView) findViewById(R.id.text);
					text.setText (clickWho.getText());
					arg0.setBackgroundResource(R.drawable.menu_bg);
					
					for(int i = 0;i < menus.length;i++){
						if( ! clickWho.getTag().equals( menuList.get(i).getText())){
							menuList.get(i).setBackgroundDrawable(null);
						}
						
					}
				}
				
			});
			menuLinerLayout.addView(tvMenu,menuLinerLayoutParames);
        }
	}

}
